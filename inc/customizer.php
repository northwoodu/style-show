<?php
/**
 * Style Show Theme Customizer
 *
 * @package Style Show
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function style_show_customize_register( $wp_customize ) {
	// $wp_customize->get_setting('blogname')->transport = 'postMessage';
	// $wp_customize->get_setting('blogdescription')->transport = 'postMessage';
	// $wp_customize->get_setting('header_textcolor')->transport = 'postMessage';

	/***********************
	 * Add Style Show Enabler
	 ***********************/
	$wp_customize->add_section('enable-event', array(
		'title' => __('Enable Style Show', 'style-show'),
		'priority' => 1,
		'description' => __('Activate or deactivate whether the event specific information should be shown.', 'style-show')
	));

	// Add Event Enabler
	$wp_customize->add_setting(
		'event-enabled',
		array(
			'default' => '0'
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Control(
			$wp_customize,
			'event-enabled',
			array(
				'label'		=> __( 'Event Enabled', 'style-show' ),
				'section'	=> 'enable-event',
				'settings'	=> 'event-enabled',
				'type'		=> 'checkbox',
				'std'		=> '1'
			)
		)
	);

	/***********************
	 * Add Social Media Customizer
	 ***********************/
	$wp_customize->add_section('social-media', array(
		'title' => __('Social Media', 'style-show'),
		'priority' => 30,
		'description' => __('Enter the URL to your account for each service for the icons to appear.', 'style-show')
	));

	// Add Twitter Setting
	$wp_customize->add_setting( 'twitter' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'twitter', array(
		'label' => __( 'Twitter Handle (no @)', 'style-show' ),
		'section' => 'social-media',
		'settings' => 'twitter',
	) ) );

	// Add Facebook Setting
	$wp_customize->add_setting( 'facebook' , array( 'default' => '' ));
	$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'facebook', array(
		'label' => __( 'Facebook', 'style-show' ),
		'section' => 'social-media',
		'settings' => 'facebook',
	) ) );
}
add_action( 'customize_register', 'style_show_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function style_show_customize_preview_js() {
	wp_enqueue_script( 'style_show_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'style_show_customize_preview_js' );
